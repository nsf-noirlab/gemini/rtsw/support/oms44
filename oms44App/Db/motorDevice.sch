[schematic2]
uniq 5
[tools]
[detail]
w -56 1251 100 0 n#1 elongouts.Dir.FLNK -176 1248 64 1248 64 1632 -928 1632 -928 -192 448 -192 448 0 576 0 eoms44m.Device.SLNK
w -672 963 100 0 dir inhier.dir.P -736 960 -608 960 -608 1360 -64 1360 -64 1216 -176 1216 elongouts.Dir.VAL
w 2000 1267 100 0 vals estringouts.DeviceStrg.OUT 1984 1264 2016 1264 2016 768 480 768 junction
w -128 771 100 0 vals inhier.vals.P -736 768 480 768 480 384 576 384 eoms44m.Device.VALS
w 1680 1507 100 0 n#2 embbi.DeviceMenu.FLNK 1664 1504 1696 1504 1696 1280 1728 1280 estringouts.DeviceStrg.SLNK
w 1696 1315 100 0 n#3 embbi.DeviceMenu.VAL 1664 1312 1728 1312 estringouts.DeviceStrg.DOL
w -208 3 100 0 dbug inhier.dbug.P -736 0 320 0 320 64 576 64 eoms44m.Device.DBUG
w -80 131 100 0 simm inhier.simm.P -736 128 576 128 eoms44m.Device.SIMM
w 1312 131 100 0 lswa eoms44m.Device.LSWA 896 128 1728 128 outhier.lswa.p
w 1312 163 100 0 mip eoms44m.Device.MIP 896 160 1728 160 outhier.mip.p
w 1312 451 100 0 ack eoms44m.Device.ACK 896 448 1728 448 outhier.ack.p
w 1312 195 100 0 mpos eoms44m.Device.MPOS 896 192 1728 192 outhier.mpos.p
w 1312 387 100 0 message_link eoms44m.Device.MSGL 896 384 1728 384 outhier.msgl.p
w 1312 419 100 0 busy_link eoms44m.Device.BSYL 896 416 1728 416 outhier.bsyl.p
w -160 579 100 0 velo inhier.velo.P -736 576 416 576 416 352 576 352 eoms44m.Device.VELO
w -112 867 100 0 mode inhier.mode.P -736 864 512 864 512 416 576 416 eoms44m.Device.MODE
w -120 1187 100 0 devdir elongouts.Dir.OUT -176 1184 -64 1184 -64 960 544 960 544 448 576 448 eoms44m.Device.DIR
w 898 514 -100 0 n#4 eoms44.Device.OUT 896 512 1152 512 hwout.hwout#18.outp
s -368 1440 100 0 when directive received
s -368 1472 100 0 Trigger assembly record SLNK
s -368 1504 100 0 DEVICE RECORD BUG WORK AROUND
s 704 848 100 0 This menu for engineering testing only
[cell use]
use bd200tr -1856 -1360 100 0 bd200tr#100
xform 0 784 320
use elongouts -432 1127 100 0 Dir
xform 0 -304 1216
p -368 1072 100 0 1 OMSL:supervisory
p -368 1040 100 0 1 PV:$(top)$(dev)
use estringouts 1984 1344 100 0 DeviceStrg
xform 0 1856 1280
p 1792 1184 100 0 1 OMSL:closed_loop
p 1792 1344 100 0 1 PV:$(top)$(dev)
use embbi 896 1712 100 0 DeviceMenu
xform 0 1152 1296
p 1184 1214 100 0 1 EIST:
p 1184 1118 100 0 1 ELST:
p 1184 990 100 0 1 FFST:
p 1184 1342 100 0 1 FRST:pos4
p 1184 1022 100 0 1 FTST:
p 1184 1310 100 0 1 FVST:pos5
p 1184 1182 100 0 1 NIST:
p 1184 1438 100 0 1 ONST:pos1
p 704 1712 100 0 1 PV:$(top)$(dev)
p 1184 1246 100 0 1 SVST:pos7
p 1184 1278 100 0 1 SXST:pos6
p 1184 1150 100 0 1 TEST:
p 1184 1374 100 0 1 THST:pos3
p 1184 1054 100 0 1 TTST:
p 1184 1086 100 0 1 TVST:
p 1184 1406 100 0 1 TWST:pos2
p 1184 1470 100 0 1 ZRST:pos0
use inhier -768 976 100 0 dir
xform 0 -736 960
use inhier -768 880 100 0 mode
xform 0 -736 864
use inhier -768 592 100 0 velo
xform 0 -736 576
use inhier -768 784 100 0 vals
xform 0 -736 768
use inhier -768 144 100 0 simm
xform 0 -736 128
use inhier -768 16 100 0 dbug
xform 0 -736 0
use outhier 1760 128 100 0 lswa
xform 0 1712 128
use outhier 1760 160 100 0 mip
xform 0 1712 160
use outhier 1760 448 100 0 ack
xform 0 1712 448
use outhier 1760 416 100 0 bsyl
xform 0 1712 416
use outhier 1760 384 100 0 msgl
xform 0 1712 384
use outhier 1760 192 100 0 mpos
xform 0 1712 192
use eoms44 832 560 100 0 Device
xform 0 736 256
p 960 -128 100 0 1 ACCL:$(accl)
p 1136 -96 100 0 1 BLCO:0.2500000e+00
p 1120 256 100 0 1 BTMO:0
p 640 608 100 0 -1 DESC:Steppermotor device drive
p 640 -64 100 0 1 DTYP:OMS 8/44
p 640 -256 100 0 1 EGU:steps
p 1136 -64 100 0 1 FIVL:$(fivl)
p 640 -192 100 0 1 IALG:$(ialg)
p 640 368 100 0 0 LOLO:0.0000000e+00
p 640 -224 100 0 1 MRES:1
p 960 -320 100 0 1 MRND:1
p 960 -224 100 0 1 PHLM:$(phlm)
p 960 -256 100 0 1 PLLM:$(pllm)
p 640 -288 100 0 1 PREC:4
p 1120 288 100 0 1 PTMO:0
p 640 560 100 0 1 PV:$(top)$(dev)
p 640 -320 100 0 1 SIMM:$(simm)
p 640 -160 100 0 1 TFIL:$(dev).lut
p 992 288 100 0 1 UAPB:NO
p 656 256 100 0 1 UBSB:NO
p 960 -288 100 0 1 UEIP:NO
p 656 288 100 0 1 UPSB:NO
p 960 -192 100 0 1 VBAS:10.0
p 960 -160 100 0 1 VELO:$(velo)
p 960 -64 100 0 1 VHLM:$(vhlm)
p 960 -96 100 0 1 VLLM:10.0
use hwout 1152 471 100 0 hwout#18
xform 0 1248 512
p 1216 544 100 0 -1 val(outp):#$(maddr)
[comments]
