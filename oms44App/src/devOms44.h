
/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 1998.                        (c) 1998
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                     
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * devDeviceControl.h
 *
 * PURPOSE:
 * Publish deviceControl record OMS8/44 device support public information
 * for devDeviceControl module.
 *
 *INDENT-OFF*
 * $Log: gcalDevDeviceControl.h,v $
 * Revision 1.3  2005/06/14 15:29:08  gemvx
 *
 * Modified Files:
 * adl/gcalMechCommands.adl
 * capfast/gcalLamps.sch
 * capfast/gcalLampsMenu.sch
 * capfast/gcalMechanisms.sch
 *  Added Files:
 *  	src/deviceControl/Makefile src/deviceControl/Makefile.Vx
 *  	src/deviceControl/ddrMessageLevels.h
 *  	src/deviceControl/deviceControlRecord.dbd
 *  	src/deviceControl/drvOmsVme.c src/deviceControl/drvOmsVme.h
 *  	src/deviceControl/gcalDevDeviceControl.c
 *  	src/deviceControl/gcalDevDeviceControl.h
 *  	src/deviceControl/newdev.dbd
 *  	src/deviceControl/recDeviceControl.c
 *  	src/deviceControl/recDeviceControl.h
 * ----------------------------------------------------------------------
 *
 * Revision 1.1  2001/08/10 14:01:48  ptaylor
 * Restructured src directory with 4 sub-directories, including pv and lut which were previously in pv as well as deviceControl and gcal previously in src
 *
 * Revision 1.1.1.1  2000/11/01 01:16:07  ptaylor
 * Initial version before test at MK
 *
 * Revision 1.1  2000/08/29 10:49:52  pbt
 * Renamed device support modules for GCAL specific OMS/8 support
 *
 * Revision 1.1.1.1  2000/08/23 14:03:36  pbt
 * As tested on August 14 2000
 *
 * Revision 1.3  2000/07/06 17:25:59  gmos
 * Williams new code, tested and debugged
 *
 * Revision 1.2  2000/04/14 10:00:35  gmos
 * Merged pre and post FP software
 *
 * Revision 1.1.1.1  2000/02/29 11:42:26  gmos
 * V1.00 release from HIA
 *
 * Revision 1.4  1999/08/13 19:24:25  angelic
 * remove setLimts from DSET
 *
 * Revision 1.3  1999/04/28 20:27:58  dunn
 * Bug fixes.
 *
 * Revision 1.2  1999/02/26 20:44:08  rambold
 * beta version
 *
 * Revision 1.1  1998/11/20 03:19:01  rambold
 * Initial revision
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef DEV_OMS44_INC
#define DEV_OMS44_INC

#define DDR_MSG_FATAL           DDR_DBUG_QUIET   /* Fatal error messages       */
#define DDR_MSG_ERROR           DDR_DBUG_NONE    /* Serious error messages     */
#define DDR_MSG_WARNING         DDR_DBUG_NONE    /* Warning messages           */
#define DDR_MSG_LOG             DDR_DBUG_MIN     /* Log messages               */
#define DDR_MSG_MIN             DDR_DBUG_MIN     /* Minimal debugging messages */
#define DDR_MSG_FULL            DDR_DBUG_FULL    /* Full debugging messages    */
#define DDR_MSG_MAX             DDR_DBUG_MAX     /* Insane debugging messages  */

/* 
 * Define the epics deviceControl record device support access structure.
 */

typedef struct {
    long            number;
    DEVSUPFUN       devReport;
    DEVSUPFUN       init;
    DEVSUPFUN       initDeviceSupport;
    DEVSUPFUN       getIointInfo;
    DEVSUPFUN       configureDrive;
    DEVSUPFUN       controlPower;
    DEVSUPFUN       controlMotion;
    DEVSUPFUN       setDelay;
    DEVSUPFUN       setPosition;
} OMS44_DSET;


/* 
 * Define the epics binary output device support access structure.
 */

typedef struct {
    long            number;
    DEVSUPFUN       report;
    DEVSUPFUN       init;
    DEVSUPFUN       initRecord;
    DEVSUPFUN       getIointInfo;
    DEVSUPFUN       writeBo;
} DEVICE_BO_OMS44_DSET;


#endif

